#!/bin/bash

source ./env

# Download all public_* files with rysnc over the SSH protocol
rsync -aP -e "ssh -i ${HOME}/.ssh/${ssh_key}" ${user}'@envs.net:~/public_*'  .

