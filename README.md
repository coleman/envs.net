# envs.net

A couple of scripts for writing and coding on envs.net


## Instructions

1. Fork this repository to make it your own.
2. Update the **env** file with your username and the name of your ssh private
   key that you use to access envs.net

Then you can download everything that's up there in your home directory.

```
./download.sh
```

Make changes locally, and publish your words and/or code.

```
./upload.sh
```

You can add and commit the "public" directories to your forked git repo, if you want.

