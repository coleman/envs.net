#!/bin/bash

source ./env

# Upload the public_* directories to your envs.net home
rsync -aP -e "ssh -i ${HOME}/.ssh/${ssh_key}" ./public_* ${user}@envs.net:~/

